﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       double zuZahlenderBetrag; 
       double rückgabebetrag;
       int anzahl;

       System.out.println("Gebe Sie die Anzahl der Tickets ein:");
       
       anzahl = tastatur.nextInt();
       
       if (anzahl > 10 || anzahl <1)
       {
    	   System.out.println("Sie haben eine ungültige Anzahl angegeben. Bitte starten Sie den Bestellvorgang neu. Ansonsten wird die Bestellung mit 1 Ticket fortgesetzt.");
       }
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       fahrkartenAusgeben();
       
     
    
       if(rückgabebetrag > 0.0)
       {
    	   rueckgeldAusgeben(rückgabebetrag);
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    public static double fahrkartenbestellungErfassen()
    {          
    	Scanner tastatur = new Scanner(System.in);
    
    	double x; 
    	System.out.print("Zu zahlender Betrag (EURO): ");
    	x = tastatur.nextDouble();
    	return x;
    }
    
    public static double fahrkartenBezahlen (double zuZahlen)
    {
    	double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
        double rückgabebetrag;
    	Scanner tastatur = new Scanner(System.in);
    	
    	  eingezahlterGesamtbetrag = 0.0;
          while(eingezahlterGesamtbetrag < zuZahlen)
          {
       	   System.out.println("Noch zu zahlen: " + (zuZahlen - eingezahlterGesamtbetrag));
       	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
       	   eingeworfeneMünze = tastatur.nextDouble();
       	   
              eingezahlterGesamtbetrag += eingeworfeneMünze;
          }
          rückgabebetrag = eingezahlterGesamtbetrag - zuZahlen;
          
          return rückgabebetrag;



    }
    public static String fahrkartenAusgeben()
    {
    	   System.out.println("\nFahrschein wird ausgegeben");
           for (int i = 0; i < 8; i++)
           {
              System.out.print("=");
              try {
    			Thread.sleep(250);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
           }
           System.out.println("\n\n");
           String x = "";
           return x;
    
    }
    public static String rueckgeldAusgeben(double x)
    {
    	System.out.println("Der Rückgabebetrag in Höhe von " + x + " EURO");
 	   System.out.println("wird in folgenden Münzen ausgezahlt:");

        while(x >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 EURO");
	          x -= 2.0;
        }
        while(x >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 EURO");
	          x -= 1.0;
        }
        while(x >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("50 CENT");
	          x -= 0.5;
        }
        while(x >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("20 CENT");
	          x -= 0.2;
        }
        while(x >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("10 CENT");
	          x -= 0.1;
        }
        while(x >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("5 CENT");
	          x -= 0.05;
        }
    	String y = "";       
    	return y;
    }
    
}