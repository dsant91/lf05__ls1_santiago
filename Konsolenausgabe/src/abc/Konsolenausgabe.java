package abc;

public class Konsolenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Aufgabe 1

		 System.out.println("Hallo Welt.\n\"Wie geht es euch?\""); //Unterschied zwischen print() und println(): println() fügt ein  Zeilenvorschub ein
		 
		 
		 //Aufgabe 2
		 
		 String s = "*************";
		 System.out.printf( "%15.1s\n", s );
		 System.out.printf( "%16.3s\n", s );
		 System.out.printf( "%17.5s\n", s );
		 System.out.printf( "%18.7s\n", s );
		 System.out.printf( "%19.9s\n", s );
		 System.out.printf( "%20.11s\n", s );
		 System.out.printf( "%21.13s\n", s );
		 System.out.printf( "%16.3s\n", s );
		 System.out.printf( "%16.3s\n", s );

		 //Aufgabe 3

		 double a = 22.4234234;
		 double b = 111.2222;
		 double c = 4.0;
		 double d = 1000000.551;
		 double e = 97.34;
		 
		 
		 System.out.printf( "%.2f\n" ,a);
		 System.out.printf( "%.2f\n" ,b);
		 System.out.printf( "%.2f\n" ,c);
		 System.out.printf( "%.2f\n" ,d);
		 System.out.printf( "%.2f\n" ,e);
		
		 

	}
	
}
