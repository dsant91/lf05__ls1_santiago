package abc;

public class Konsolenausgabe2 {

	public static void main(String[] args) {
		// Aufgabe 1
		String s = "*************";
		System.out.printf("%5.2s\n", s);
		System.out.printf("*%7.1s\n", s);
		System.out.printf("\n*%7.1s\n", s);
		System.out.printf("%5.2s\n\n", s);

		//Aufgabe 2

		String x = " 1 * 2 * 3 * 4 * 5 ";
		
		
	
		System.out.printf("%-5s=", "0!");
		System.out.printf("%-19.0s=", x);
		System.out.printf("%4s\n", "1");
		
		System.out.printf("%-5s=", "1!");
		System.out.printf("%-19.2s=", x);
		System.out.printf("%4s\n", "1");

		System.out.printf("%-5s=", "2!");
		System.out.printf("%-19.6s=", x);
		System.out.printf("%4s\n", "2");

		System.out.printf("%-5s=", "3!");
		System.out.printf("%-19.10s=", x);
		System.out.printf("%4s\n", "6");

		System.out.printf("%-5s=", "4!");
		System.out.printf("%-19.14s=", x);
		System.out.printf("%4s\n", "24");

		System.out.printf("%-5s=", "5!");
		System.out.printf("%-19.18s=", x);
		System.out.printf("%4s\n", "120");



		System.out.println("\n");
		
		
		
		

		//Aufgabe 3
		double a = -28.8889;
		double b = -23.3333;
		double c = -17.7778;
		double d = -6.6667;
		double e = -1.1111;

		System.out.printf("%-12s|", "Fahrenheit");
		System.out.printf("%10s\n", "Celsius");
		System.out.println("-----------------------");
		
		System.out.printf("%-12s|", "-20");		
		System.out.printf("%10.2f\n", a);

		System.out.printf("%-12s|", "-10");		
		System.out.printf("%10.2f\n", b);
		
		System.out.printf("%-12s|", "0");		
		System.out.printf("%10.2f\n", c);
		
		System.out.printf("%-12s|", "+10");		
		System.out.printf("%10.2f\n", d);
		
		System.out.printf("%-12s|", "+2l0");		
		System.out.printf("%10.2f\n", e);




	

	}

}