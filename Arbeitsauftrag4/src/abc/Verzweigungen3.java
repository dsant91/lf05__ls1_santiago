package abc;
import java.util.Scanner;

public class Verzweigungen3 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); 
		System.out.println("Bitte geben Sie die Menge an die zu bestellenden PC-Mäuse ein:");
		int x = myScanner.nextInt();
		
		if (x >= 10) {
			System.out.println("Sie haben "+ x + " PC-Mäuse mit dem Einzelpreis von 22€ bestellt. Der Gesamtpreis beträgt " + (22 * x)+"€");
		}
		if (x <10 && x != 0) {
			
			System.out.println ("Sie haben " + x + " PC-Mäuse mit dem Einzelpreis von 22€ bestellt. Der Gesamtpreis inklusive Liefergebühr beträgt " + (22 * x + 10) + " €");
		}
		else
			System.out.println("Fehlerhafte Eingabe");


	}

}
