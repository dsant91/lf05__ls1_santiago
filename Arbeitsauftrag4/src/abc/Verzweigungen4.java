package abc;
import java.util.Scanner;

public class Verzweigungen4 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); 
		System.out.println("Bestellwert eingeben:");
		double x = myScanner.nextDouble();

		if (x > 0 && x <= 100) {
			
			x = x - x * 0.1;
			x = x + x * 0.19;
			System.out.println("Der ermäßigte Bestellwert beträgt " + x + "€");
		}
		else if (x > 100 && x <= 500 ) {
			x = x - x * 0.15;
			x = x + x * 0.19;
			System.out.println("Der ermäßigte Bestellwert beträgt " + x + "€");

			
		}
		else {
			x = x - x * 0.20;
			x = x + x * 0.19;
			System.out.println("Der ermäßigte Bestellwert beträgt " + x + "€");
		}
	}

}
