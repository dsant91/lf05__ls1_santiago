package abc;
import java.util.Scanner;

public class Verzweigungen2 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in); 
		System.out.println("Bitte geben Sie den Nettowert ein:");
		double preis = myScanner.nextDouble();
		
		System.out.println("Bitte geben Sie j für den ermäßigten Steuersatz oder n für den vollen Steuersatz ein:");
		char x = myScanner.next().charAt(0);
		
		if (x == 'j') {
			preis =preis + preis * 0.15;
			System.out.println("Der Bruttobetrag mit dem ermäßigten Steuersatz beträgt "+ preis);

		}
		if (x == 'n' ) {
			preis = preis + preis * 0.19;
			System.out.println("Der Bruttobetrag mit dem vollen Steuersatz beträgt "+ preis);

		}
		//else
			//System.out.println("Fehler bei der Eingabe");//

	}

}
