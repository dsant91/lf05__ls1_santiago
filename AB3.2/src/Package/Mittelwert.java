
  package Package;
  
  
  public class Mittelwert {
  
  public static void main(String[] args) {
  
  // (E) "Eingabe" // Werte für x und y festlegen: //
 double x = 2.0; 
 double y = 4.0;
  
  
  // (V) Verarbeitung // Mittelwert von x und y berechnen: //
  
  // (A) Ausgabe // Ergebnis auf der Konsole ausgeben: //

  System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, berechneMittelwert(x,y)); } 
  
  public static double berechneMittelwert (double s, double i) {
  
  double x = (s + i) / 2; 
  return x;
  
  
  }
  
  
  }
 