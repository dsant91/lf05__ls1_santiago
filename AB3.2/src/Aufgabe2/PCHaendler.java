package Aufgabe2;

import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		String artikel = liesString("was möchten Sie bestellen?");
	
		int anzahl = liesInt("Geben Sie die Anzahl ein:");

		double preis = liesDouble ("Geben Sie den Nettopreis ein:");

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = myScanner.nextDouble();

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis (nettogesamtpreis, mwst);

		// Ausgeben

		rechnungausgeben (artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}
	public static String liesString(String text)
	{
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);

		String  x = myScanner.next();
		return x;

	}
	public static int liesInt(String text)
	{
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);

		int  x = myScanner.nextInt();
		return x;
		
	}
	public static double liesDouble(String text) 
	{	
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double  x = myScanner.nextDouble();
		return x;
	}
	 public static double berechneGesamtnettopreis(int anzahl, double nettopreis) 
	 {
		 double x = anzahl * nettopreis;
		 return x;
	 }
	 public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) 
	 {
		double x = nettogesamtpreis * (1 + mwst / 100);
		return x;  
	 }
	 public static void rechnungausgeben(String artikel, int anzahl, double
			 nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		
		 
		 	System.out.println("\tRechnung");
		 	System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
			System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl,nettogesamtpreis);
			
		
		 
	 }
			 
}